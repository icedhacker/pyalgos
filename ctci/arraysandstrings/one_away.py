"""
Chapter 1 - Problem 1.5

There are three types of edits that can be performed on strings :
Insert a character
Remove a character
Replace a character

Given 2 strings, write a function to check if they are one edit (or zero edits) away.

Example :
pale, ple -> True
pales, pale -> True
pale, bale -> True
pale, bake -> False
"""

def one_away(A, B):
    sizeA = len(A)
    sizeB = len(B)
    if abs(sizeB - sizeA) > 1:
        return False
    if sizeA > sizeB:
        return check_insert(B, A)
    elif sizeB > sizeA:
        return check_insert(A, B)
    else:
        return check_replace(A, B)

# This function checks if A --> B with one insert.
# Assumes len(A) < len(B)
def check_insert(A, B):
    j = 0
    i = 0
    miss_count = 0
    while i < len(A) and j < len(B):
        if A[i] == B[j]:
            i = i + 1
            j = j + 1
        else:
            miss_count = miss_count + 1
            j = j + 1
    if miss_count > 1:
        return False
    elif miss_count == 1 and i < len(A):
        return False
    return True

def check_replace(A, B):
    miss_count = 0
    for i in range(0, len(A)):
        if A[i] != B[i]:
            miss_count = miss_count + 1
        i = i + 1
    if miss_count > 1:
        return False
    return True

A = "pales"
B = "palk"
print(one_away(A, B))
