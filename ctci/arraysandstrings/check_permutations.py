"""
Chapter 1 - Problem 1.2

Given 2 strings, write a method to decide if 1 string is a permutation of the other.

Example : 
Input : sanath, thansa
Output : True
"""

def check_permutations(A, B):
    if len(A) != len(B):
        return False
    alphabets = {}
    for val in A:
        if val in alphabets:
            alphabets[val] = alphabets[val] + 1
        else:
            alphabets[val] = 1
    for val in B:
        if val in alphabets:
            alphabets[val] = alphabets[val] - 1
            if alphabets[val] < 0:
                return False
        else:
            return False
    return True

A = 'sanath'
B = 'thansa'
print(check_permutations(A, B))
