"""
Chapter 1 - Problem 1.3

Write a method to replace all spaces in a string with '%20'.

You may assume that the string has sufficient space at the end to hold the additional characters,
and that you are given the "true" length of the string.
(Note: If implementing in Java, please use a character array so that you can perform this operation in place.)

Example:
Input : "Mr John Smith    ", 13
Output : "Mr%20John%20Smith"
"""

def urlify(A, N):
    if N == 0 or A is None or A == '':
        return A
    j = 1
    i = len(A) - 1
    A = list(A)
    while i >= 0 and (N - j) >= 0:
        if A[N - j] == ' ':
            A[i] = '0'
            A[i - 1] = '2'
            A[i - 2] = '%'
            i = i - 3
        else:
            A[i] = A[N - j]
            i = i - 1
        j = j + 1
    return "".join(A)

A = "Mr John Smith    "
N = 13
print(urlify(A, N))