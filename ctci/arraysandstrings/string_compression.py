"""
Chapter 1 - Problem 1.6

Implement a method to perform basic string compression using the counts of repeated characters.

For example, the string aabcccccaaa would become a2b1c5a3.
If the "compressed" string would not become smaller than the original string, your method should return the original string.

You can assume the string has only upper or lower case letters.

Example : 
Input : aabcccccaaa
Output : a2b1c5a3
"""

def string_compression(A):
    result = ''
    result_length = 0
    sizeA = 0
    current_alpha = ''
    current_count = 0
    for alpha in A:
        sizeA = sizeA + 1
        if current_alpha != alpha:
            if result_length == 0:
                result = result + alpha
                result_length = result_length + 1
            else:
                result = result + str(current_count)
                result = result + alpha
                result_length = result_length + 2
            current_alpha = alpha
            current_count = 1
        else:
            current_count = current_count + 1
    result = result + str(current_count)
    result_length = result_length + 1
    if result_length < sizeA:
        return result
    else:
        return A

A = "abccaaa"
print(string_compression(A))