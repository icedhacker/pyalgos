"""
Chapter 1 - Problem 1.1
Implement an algorithm to determine if a string has all unique characters. 
What if you cannot use additional data structures ?

Example : 
Input : SANATH
Output : False
"""

def is_unique(A):
    a_alphabets = {}
    for val in A:
        if val in a_alphabets:
            return False
        else:
            a_alphabets[val] = 1
    return True

def is_unique_no_ds(A):
    A = ''.join(sorted(A))
    for i in range(0, len(A) - 1):
        if A[i] == A[i + 1]:
            return False
    return True

A = 'sanath'
B = 'santh'
print(is_unique(A))
print(is_unique(B))
print(is_unique_no_ds(A))
print(is_unique_no_ds(B))