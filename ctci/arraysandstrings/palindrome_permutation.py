"""
Given a string, write a function to check if it is a permutation of a palindrome. 
A palindrome is a word or phrase that is the same forwards and backwards.
A permutation is a rearrangement of letters. The palindrome doesn't need to limited to just dictionary words.

Example : 
Input : Tact Coa
Output : True ["taco cat", "atco cta",..]

Palindrome conditions : 
If the length(str) is even, then the count of each alphabet in this string should be even.
If the length(str) is odd, then the count of all should be even except one which should be odd.  
"""

def palindrome_permutation(S):
    size = len(S)
    S = S.lower()
    alpha_dict = {}
    for alphabet in S:
        if alphabet == ' ':
            size = size - 1  
        elif alphabet in alpha_dict:
            alpha_dict[alphabet] = alpha_dict[alphabet] + 1
        else:
            alpha_dict[alphabet] = 1
    no_of_odds = 0
    for key in alpha_dict.keys():
        if alpha_dict[key] % 2 != 0:
            no_of_odds = no_of_odds + 1
    if size % 2 == 0:
        if no_of_odds == 0:
            return True
        else:
            return False
    else:
        if no_of_odds == 1:
            return True
        else:
            return False

S = "Tact Coa"
print(palindrome_permutation(S))
