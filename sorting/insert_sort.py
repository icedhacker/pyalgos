"""
Implement the insert sort algorithm.

Insert Sort : 
Maintain a sorted sublist
While looping through the numbers, 
Add numbers to the sublist
Sort the newly added number in the sublist

Time Complexity : O(n^2)
Space Complexity : O(1)
"""

def insert_sort(array):
    sorted_index = 0
    for i in xrange(sorted_index + 1, len(array)):
        tmp_index = i
        for j in xrange(i - 1, -1, -1):
            if (array[tmp_index] > array[j]):
                break
            tmp = array[tmp_index]
            array[tmp_index] = array[j]
            array[j] = tmp
            tmp_index = j
    return array

a = [3, 44, 38, 5, 47, 15, 36, 26, 27, 2, 46, 4, 19, 50, 48]
print a
print insert_sort(a)
