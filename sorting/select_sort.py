"""
Implement the select sort algorithm.

Select Sort : 
Loop through the numbers
Find the smallest number
swap with the lowest index in unsorted array
Repeat

Time Complexity : O(n^2)
Space Complexity : O(1)
"""

def select_sort(array):
    sorted_index = 0
    while sorted_index < (len(array)-1):
        min_index = sorted_index
        for i in xrange(sorted_index + 1, len(array)):
            if array[i] < array[min_index]:
                min_index = i
        tmp = array[sorted_index]
        array[sorted_index] = array[min_index]
        array[min_index] = tmp
        sorted_index = sorted_index + 1
    return array

a = [3, 44, 38, 5, 47, 15, 36, 26, 27, 2, 46, 4, 19, 50, 48]
print a
print select_sort(a)
