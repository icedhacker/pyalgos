"""
Implement the bubble sorting algorithm

Bubble Sort : 
Loop through the numbers
Compare 2 adjacent indices and swap if left > right
Repeat

Time Complexity : O(n^2)
Space Complexity : O(1)
"""

def bubble_sort(array):
    sorted_index = len(array)
    while sorted_index != 0:
        for i in xrange(sorted_index - 1):
            if array[i] > array[i + 1]:
                tmp = array[i]
                array[i] = array[i + 1]
                array[i + 1] = tmp
        sorted_index = sorted_index - 1
    return array

a = [3, 44, 38, 5, 47, 15, 36, 26, 27, 2, 46, 4, 19, 50, 48]
print a
print bubble_sort(a)
