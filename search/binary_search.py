"""
Implement the Binary Search algorithm.

Assumptions : The array is sorted.
"""

def binary_search(array, key):
    left = 0
    right = len(array) - 1
    if array[left] == key:
        return left
    while left < right:
        middle = (left + right) / 2
        if array[middle] == key:
            return middle
        elif array[middle] > key:
            right = middle - 1
        else:
            left = middle + 1
    return -1

a = [3, 44, 38, 5, 47, 15, 36, 26, 27, 2, 46, 4, 19, 50, 48]
print binary_search(a, 15)