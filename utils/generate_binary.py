"""
Implement an algorithm to generate the binary representation for a number.
"""

def get_binary(n):
    binary_list = []
    while n > 0:
        remainder = n % 2
        n = n / 2
        binary_list.append(remainder)
    return binary_list[::-1]

n = 8
print get_binary(n)