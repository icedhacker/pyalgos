"""
Implement the algorithm to count the number of inversions in an array.

Algorithm :
Implement Merge Sort
During the merge process, count the number of inversions.
"""

count = 0
def merge_sort(array, first, last):
    if last == first:
        return [array[first]]
    array1 = merge_sort(array, first, (last + first) / 2)
    array2 = merge_sort(array, ((last + first) / 2) + 1, last)
    return merge(array1, array2)

def merge(array1, array2):
    global count
    i = 0
    j = 0
    sorted_array = []
    while i < len(array1) and j < len(array2):
        if array1[i] > array2[j]:
            count = count + len(array1) - i
            sorted_array.append(array2[j])
            j = j + 1
        else:
            sorted_array.append(array1[i])
            i = i + 1
    if i < len(array1):
        sorted_array.extend(array1[i:len(array1)])
    elif j < len(array2):
        sorted_array.extend(array2[j:len(array2)])
    return sorted_array

a = [3, 44, 38, 5, 47, 15, 36, 26, 27, 2, 46, 4, 19, 50, 48]
print a
print merge_sort(a, 0, len(a) - 1)
print count
