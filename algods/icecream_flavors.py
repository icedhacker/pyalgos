"""
Sunny and Johnny together have M dollars they want to spend on ice cream. The parlor offers n flavors,
and they want to choose two distinct flavors so that they end up spending the entire amount.
You are given the cost of these flavors.

The cost of the ith flavor is denoted by c[i].
Also, given that c[i+1] >= c[i]. You have to find the two flavors whose sum is M.

Sample Input: M = 4 cost = [1,2,3,4,5]
Output: 0, 2
Explanation: As we can see, cost[0] + cost[2] = 1 + 3 = 4
"""

def icecream_flavors(m, cost):
    start_index = 0
    last_index = len(cost) - 1
    while start_index < last_index:
        if cost[start_index] + cost[last_index] == m:
            print(start_index, last_index)
            break
        elif cost[start_index] + cost[last_index] < m:
            start_index = start_index + 1
        else:
            last_index = last_index - 1

m = 4
cost = [1,2,3,4,5]
icecream_flavors(m, cost)