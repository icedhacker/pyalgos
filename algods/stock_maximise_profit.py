"""
You are a stock broker and you are good at predicting daily stock prices.
Based on your prediction, you want to maximize your profit, for a given period of time
If you were only permitted to complete at most one transaction over that period 
(ie, buy one and sell one share of the stock), 
design an algorithm to find the maximum profit.

Sample Input: [7, 1, 5, 3, 6, 4]
Output : 5
"""

def maximise_stock_profit(stock_array):
    max_profit = 0
    min_val = stock_array[0]
    for stock_price in stock_array:
        if stock_price < min_val:
            min_val = stock_price
        else:
            tmp_profit = stock_price - min_val
            if tmp_profit > max_profit:
                max_profit = tmp_profit
    return max_profit

a = [1, 2, 3, 4, 5, 6, 7]
print maximise_stock_profit(a)