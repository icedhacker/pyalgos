"""
Implement the math.power method and optimize.

Method 1:
pow(a, n) = a * a * a * ... n times

Method 2 (Recursion):
if n is even:
    pow(a, n) = pow(a^2, n/2)
else:
    pow(a, n) = a * pow(a^2, (n-1)/2)

Method 3 (Using binary representation):
n = 2^p + 2^q + 2^r + ....
a^n = a^(2^p) * a^(2^q) * a^(2^r) * ....
"""

def power(a, n):
    product = 1
    while n > 0:
        if n & 1:
            product = product * a
        a = a * a
        n >>= 1
    return product

a = 5
n = 88
print power(a, n)