"""
Let's say you have an array with a list of sub strings.
Write a function that takes an input string and return true if the string can be divided into substrings contained in the array.
Example: array: ['and', 'the', 'back', 'front', 'ory', 'end', 'po', 'pu', 'lar']
function subDivideString(inputString):

if inputString =='backend'   return true since array has 'back' , 'end'
if inputString == 'popular' return true since array has 'po' , 'pu', 'lar'
if inputString == 'backwards'   return false since array doesnt have 'wards'
if inputString == 'swapan'   return false since array doesnt have any part of the word.
"""
def sub_divide_string(strDict, inputString, startIndex):
    if inputString in strDict:
        return True
    if startIndex >= len(inputString):
        return True
    prefix = inputString[0:startIndex]
    if startIndex == 0:
        prefix = ""
    for i in range(startIndex, len(inputString)):
        prefix = prefix + inputString[i]
        if prefix in strDict:
            if i + 1 >= len(inputString):
                return True
            return (sub_divide_string(strDict, inputString[i + 1:], 0) or sub_divide_string(strDict, inputString, i + 1))
    return False
            
def main_sub_string(A, inputString):
    strDict = {}
    for strVal in A:
        strDict[strVal] = 1
    return sub_divide_string(strDict, inputString, 0)

A = ['and', 'the', 'back', 'front', 'ory', 'end', 'po', 'popu', 'lar']
inputString = "popular"
print(main_sub_string(A, inputString))