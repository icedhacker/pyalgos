"""
Given an NxN grid of characters and a dictionary, find all words which can be made from the characters 
in grid and present in the given dictionary. A word can start and end at any character in the grid.
Next character must be adjacent to previous character in any of the directions i.e. up, down, left, right and diagonal.
Character at each position in grid can be used only once while making a word.

Suppose we have a 3x3 grid and a dictionary as input.
Six words can be made from the grid which are present in the dictionary.
Green highlighted characters indicate that they form the word 'eat' in grid which is also present in the dictionary.
In grid we start at character 'E', then move to upper diagonal for 'A' and then right to 'T' to make 'EAT'.

Input : 
C A T  
R R E
T O N

Dictionary
cat, cater, art, toon, moon, not, eat, ton

Output Words
not
cat
art
cater
ton
eat
"""