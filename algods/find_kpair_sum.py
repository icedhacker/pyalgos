"""
Find a set of k digits from the given array A whose total = sum.
"""
def findKSum(A, sum_val, k):
    if k == 0:
        return []
    if sum_val < 0:
        return []
    if len(A) == 1:
        if A[0] == sum_val:
            return [A[0]]
        else:
            return []
    valOnSelection = []
    valOnSelection.append(A[0])
    valOnSelection.extend(findKSum(A[1:], sum_val - int(A[0]), k - 1))
    sumValOnSel = 0
    for x in valOnSelection:
        sumValOnSel = sumValOnSel + x    
    valWithoutSelection = []
    valWithoutSelection.extend(findKSum(A[1:], sum_val, k))
    sumValWithoutSel = 0
    for x in valWithoutSelection:
        sumValWithoutSel = sumValWithoutSel + x
    if sumValOnSel == sum_val:
        return valOnSelection
    elif sumValWithoutSel == sum_val:
        return valWithoutSelection
    else:
        return []

A = [0, 8, 1, 5, 3, 4, 2, 9]
sum_val = 10
print(findKSum(A, sum_val, 3))